'use strict'

var deprecate = require('depd')('loopback-lets-company-scope-mixin');
var companyScope = require('./company-scope');

module.exports = function mixin(app) {
    app.loopback.modelBuilder.mixins.define = deprecate.function(app.loopback.modelBuilder.mixins.define,
        'app.modelBuilder.mixins.define: Use mixinSources instead ' +
        'see https://bitbucket.org/letscomunicadev/loopback-lets-company-scope-mixin#mixinsources')
    app.loopback.modelBuilder.mixins.define('CompanyScope', companyScope)
};