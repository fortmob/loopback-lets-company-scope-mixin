CompanyScope Loopback Mixin
=============

This module is designed for the [Strongloop Loopback](https://github.com/strongloop/loopback) framework. The mixin focus in the company scope, defining its relationship, hook before saving (adding it to user) and method to find it.

To implement copy this file to mixins folder or clone this private repository (requires adding the package folder path to mixins inside model-config json file), and add "BaseModel": true in mixins on YourModel.json file.

Author: Let's Comunica

E-mail: fabio@letscomunica.com.br

**LAST VERSION: 0.0.2**

Install
-------

```bash
  npm install --save git+ssh://git@bitbucket.org/letscomunicadev/loopback-lets-company-scope-mixin.git#v0.0.2
```

Updates
-------

Made a modification? Test it at least in one project before submiting a version. It still needs unit testing and CI with projects. After everything seems perfectly up-to-date, run the following steps:

1\. Commit and push your updates using Let's Bitbucket credentials

2\. Change and commit a new tag version (always check and update the last version here and in package.json):

```bash
$ git tag -a vX.X.X -m "version_message"
```

3\. Push the new tag version to remote repository:

```bash
$ git push origin vX.X.X  # Version needs to be the same from commit
```

4\. Run npm installation with the newest version:

```bash
  npm install --save git+ssh://git@bitbucket.org/letscomunicadev/loopback-lets-company-scope-mixin.git#vX.X.X
```

Configure
----------

To use with your Models add the `mixins` attribute to the definition object of your model config.

```json
  {
    "name": "Widget",
    "properties": {
      "name": {
        "type": "string",
      },
    },
    "mixins": {
      "CompanyScope" : true,
    },
  },
```